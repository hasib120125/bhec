<?php include('inc/header.php');?>

<div class="container" style="margin-top: 10px;margin-bottom: 10px;">
  <div class="row">
      <div class="col-sm-8">
      <div class="row mp">
        <div class="panel-group">
            <div class="panel panel-primary">
              <div class="panel-heading p-head">Vice-Principle Message</div>
              <div class="panel-body">
                  <div class="col-md-12">                 
                    <div class="col-sm-12" align="center">
                        <img src="images/chairman.jpeg" class="img-responsive" alt="Brig Gen Iqbal Ahmed, ndc, afwc, psc" title="Brig Gen Iqbal Ahmed, ndc, afwc, psc">                    
                    </div>
                    <div class="row">
                      <div class="col-md-12 padding-top10 txt-g">
                        <br>
                        <p>I am very glad to know that SAGC has launched its own official website. I take this opportunity to thank all concerned for their extreme effort to make this possible.</p>

                        <p>The SAGC is an icon of excellence. We want to share our activities with the students and the people of our country. The launch of this website, therefore, is a step in the direction to help us share with the rest of the world. It has offered us room to join the ranks of modern learning throughout the world.</p>

                        <p>Today SAGC stands as one of the magnificent institutions in the country and has become synonymous with academic excellence. The college is consistently ranked among the top colleges in the country.</p>

                        <p>This SAGC bears testimony to the mission and vision of the demand of new era. It has been a milestone for measuring academic, co curricular and extracurricular excellence possessing a positive sense of responsibility to the society.</p>

                        <p>It unfolds the mission of SAGC from its inception. United we might strive the tough goals and break all shackles on the way to improvement and contribution. SAGC sends out some of the brightest students into the world, who carry the flags of the college wherever they go.</p>

                        <p>I hope that SAGC will continue to enlighten women to assume leadership roles, become concerned citizens to remove darkness, who will contribute to the world of knowledge and work. Hopefully we will ensure that most of the information is posted and updated.</p>

                        <p>May Allah bless us all.</p>  

                        <p>&nbsp;</p>               
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12 padding-top10 txt-g">
                        <div class="col-sm-6"></div> 
                        <div class="col-sm-6">
                            <p><strong>Brig Gen Iqbal Ahmed, ndc, afwc, psc</strong><br>Chairman<br>SHAHEED BIR UTTAM LT ANWAR GIRLS' COLLEGE<br> Dhaka Cantonment.</p>
                        </div>
                      </div>
                    </div>
                </div>  
              </div>
            </div>
        </div>

       <div class="row mp" style="color: #736AFF;font-weight: bold;">
          <div class="col-md-12 box" style="margin-top: 10px;">
            Total Views : 2369 views
          </div>
        </div>
    </div>

  </div>

    <div class="col-sm-4 details-right-panel">
      <div class="row mp">
          <div class="col-xs-12 col-sm-12">
              <div class="row">
                    <div class="bs-breadcrumb">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home" aria-hidden="true" style="color: white;font-size: 20px"></i></a></li>
                            <li><a href="#">Administration</a></li>
                            <li><a href="#">Message VP</a></li>
                        </ul>
                    </div>
                </div>
              <div class="row">
                  <div class="well well-heading"><i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video</div>
              </div>
              <div class="row text-center margin-top10">
                  <iframe width="360" height="200" src="//www.youtube.com/embed/qEF1odHJgKI" frameborder="0" allowfullscreen>
                  </iframe>
              </div>

              <div class="panel-group">
                <div class="panel panel-primary" style="margin: 0px -11px;">
                  <div class="panel-heading">Messages</div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-xs-12 col-sm-12">
                          <a href="message-principle.php"><div class="col-sm-4"><img src="images/chairman.jpeg" class="img-responsive  img-thumbnail" alt="Colonel Mohammad Wahidur Rahman, afwc,psc" title="Colonel Mohammad Wahidur Rahman, afwc,psc"></div>
                          <div class="col-sm-8 mp"><span style="font-weight:bold;">Message From Principle<br>Colonel Mohammad Wahidur Rahman, afwc,psc</span><br>Principal</div></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                  <div class="well well-heading"><i class="fa fa-windows" aria-hidden="true"></i> Related Topics</div>
              </div>

              <div class="row margin-bottom20">
                  <ul class="list-unstyled">
                    <li class="list-group-item"><a href="governing-body.php"><i class="fa fa-newspaper-o"></i> Governing Body</a></li>
                    <li class="list-group-item"><a href="message-chairman.php"><i class="fa fa-history"></i> Message Chairman</a></li>
                    <li class="list-group-item"><a href="message-principle.php"><i class="fa fa-newspaper-o"></i> Message Principal</a></li>
                    <li class="list-group-item"><a href="teacher-info.php"><i class="fa fa-newspaper-o"></i> Teacher Information</a></li>
                    <li class="list-group-item"><a href="staff-info.php"><i class="fa fa-newspaper-o"></i> Staff Information</a></li>
                  </ul>                   
                  </div>
            </div>
        </div>              
    </div>
  </div>
</div>
</div>



<?php include('inc/footer.php');?>