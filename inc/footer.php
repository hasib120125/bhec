  <head>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title >Bangladesh Home Economics College 1996</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>

<footer class="nb-footer">
<div class="container">
  <div class="row">
    <div class="col-sm-4">
      <div class="footer-info-single">
        <h2 class="title">Quick Links</h2>
        <ul class="list-unstyled">
          <li><a href="#" title=""><i class="fa fa-angle-double-right"></i> Results</a></li>
          <li><a href="#" title=""><i class="fa fa-angle-double-right"></i> Admission</a></li>
          <li><a href="#" title=""><i class="fa fa-angle-double-right"></i> Administration</a></li>
          <li><a href="#" title=""><i class="fa fa-angle-double-right"></i> Notice</a></li>
          <li><a href="#" title=""><i class="fa fa-angle-double-right"></i> Syllabus</a></li>
        </ul>
      </div>
    </div>

    <div class="col-sm-4">
      <div class="about">
        <div class="social-media">
          <ul class="list-inline">
            <li><a href="#" title=""><i class="fa fa-facebook" style="margin-top: 12px"></i></a></li>
            <li><a href="#" title=""><i class="fa fa-twitter" style="margin-top: 12px"></i></a></li>
            <li><a href="#" title=""><i class="fa fa-google-plus" style="margin-top: 12px"></i></a></li>
            <li><a href="#" title=""><i class="fa fa-linkedin" style="margin-top: 12px"></i></a></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="col-sm-4">
      <div class="footer-info-single">
        <h2 class="title">Our Address</h2>
        <p>Sample HTML page with Twitter's Bootstrap. Code example of Easy Sticky Footer using HTML, Javascript, jQuery, and CSS. This bootstrap tutorial covers all the major elements of responsive</p>
      </div>
    </div>

  </div>

</div>

<section class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <p>Copyright © <?php echo date('Y');?>- Bangladesh Home Economics College All Rights Reserved. Developed by <a href="http://deshuniversal.com/" target="_blank">Desh Universal (Pvt.) Limited.</a></p>
      </div>
    </div>
  </div>
</section>
</footer>