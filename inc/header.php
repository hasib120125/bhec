  <head>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title >Bangladesh Home Economics Collage 1996</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/photo-details.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            $(".dropdown").hover(            

                function() {

                    $('.dropdown-menu', this).not('.in .dropdown-menu').stop( true, true ).slideDown("fast");

                    $(this).toggleClass('open');        

                },

                function() {

                    $('.dropdown-menu', this).not('.in .dropdown-menu').stop( true, true ).slideUp("fast");

                    $(this).toggleClass('open');       

                }

            );

        });
    </script>

  </head>

<header>
    <div class="container">
      <div class="row header_section">
            <div class="col-md-3 col-sm-2 col-xs-12 logo_area">
              <a href="index.php">
                <img src="images/bhec_logo.jpg" alt="bhec_logo" height="140px"/>
              </a>
            </div>

            <div class="col-md-9 col-sm-10 col-xs-12">
              <div class="header_title">
                <h1><a href="#" target="_blank">Bangladesh Home Economics Collage, Dhaka.</a></h1>
              </div>
              <div class="header_subtitle">
                <h5>বাংলাদেশ গার্হস্থ্য অর্থনীতি কলেজ</h5>
                <h6>প্রতিষ্ঠিত: ১৯৯৬</h6>
              </div>
            </div>
      </div>
    <div class="row">
            <div class="col-sm-12">
            <nav class="navbar" style="background: #1d1dbf;margin-bottom: 0px !important">
            <!-- Brand and toggle get grouped for better mobile display -->
           <div class="navbar-header">
                <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle" style="background-color: white !important">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar" style="background-color: blue !important"></span>
                    <span class="icon-bar" style="background-color: blue !important"></span>
                    <span class="icon-bar" style="background-color: blue !important"></span>
                </button>
            </div>
            <!-- Collection of nav links, forms, and other content for toggling -->
            <div id="navbarCollapse" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php"><i class="fa fa-home" aria-hidden="true" style="font-size: 20px;"></i></a></li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">About Us <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="details.php"><i class="fa fa-globe"></i> At a Glance</a></li>
                            <li><a href="history.php"><i class="fa fa-history"></i> History</a></li>
                            <li><a href="achievement.php"><i class="fa fa-newspaper-o"></i> Achievements</a></li>
                            <li><a href="infrastructure.php"><i class="fa fa-university"></i> Infrastructure</a></li>
                            <li><a href="news-and-events.php"><i class="fa fa-newspaper-o"></i> News & Events</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Administration <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="governing-body.php"><i class="fa fa-users"></i> Governing Body</a></li>
                            <li><a href="message-chairman.php"><i class="fa fa-file-text-o"></i> Message Chairman</a></li>
                            <li><a href="message-principle.php"><i class="fa fa-file-text-o"></i> Message Principal</a></li>
                            <li><a href="message-vice-principle.php"><i class="fa fa-file-text-o"></i> Message Vice Principal</a></li>
                            <li><a href="teacher-info.php"><i class="fa fa-user"></i> Teacher Information</a></li>
                            <li><a href="staff-info.php"><i class="fa fa-user"></i> Staff Information</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Admission <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="admission-circular.php"><i class="fa fa-th-large"></i> Admission Circular</a></li>
                            <li><a href="prospectus.php"><i class="fa fa-file-o"></i> Prospectus</a></li>
                            <li><a href="admission-form.php"><i class="fa fa-file-text"></i> Admission Form</a></li>
                            <li><a href="admission-result.php"><i class="fa fa-columns"></i> Admission Result</a></li>
                            <li><a href="course-and-programs.php"><i class="fa fa-object-group"></i> Courses/Programs</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="">Department <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="food-and-nutrition.php">Food & Nutrition</a></li>
                            <li><a href="child-development.php">Child Development & Social Relationship</a></li>
                            <li><a href="resource-management.php">Resource Management & Entrepreneurship</a></li>
                            <li><a href="arts-creative.php">Art & Creative Studies</a></li>
                            <li><a href="clothing-textile.php">Clothing & Textile</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="notice.php" >Notice <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="notice.php"><i class="fa fa-bell"></i> General</a></li>
                            <li><a href="notice.php"><i class="fa fa-bell"></i> Food & Nutrition</a></li>
                            <li><a href="notice.php"><i class="fa fa-bell"></i> Child Development & Social Relationship</a></li>
                            <li><a href="notice.php"><i class="fa fa-bell"></i> Resource Management & Entrepreneurship</a></li>
                            <li><a href="notice.php"><i class="fa fa-bell"></i> Art & Creative Studies</a></li>
                            <li><a href="notice.php"><i class="fa fa-bell"></i> Clothing & Textile</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Syllabus <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="syllabus.php">General</a></li>
                            <li><a href="syllabus.php">Food & Nutrition</a></li>
                            <li><a href="syllabus.php">Child Development & Social Relationship</a></li>
                            <li><a href="syllabus.php">Resource Management & Entrepreneurship</a></li>
                            <li><a href="syllabus.php">Art & Creative Studies</a></li>
                            <li><a href="syllabus.php">Clothing & Textile</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Gallery <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="photo-gallery.php"><i class="fa fa-camera"></i> Photo Gallery</a></li>
                            <li><a href="video-gallery.php"><i class="fa fa-file-video-o"></i> Video Gallery</a></li>
                        </ul>
                    </li>

                    <li class="active"><a href="contact-us.php">Contact Us</a></li>

                    <li class="active"><a href="#">Carrier</a></li>

                </ul>
            </div>
        </nav>
            </div>
    </div>

    </div>
</header>
