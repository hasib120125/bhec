<?php include("inc/header.php");?>

<div class="container">
     <div class="row margin-top10 mp">
         <div class="col-sm-8 details-left-panel">
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading p-head">Bangladesh Home Economics Collage Photo Gallery</div>
                    <div class="panel-body">
                        <div class="col-sm-4 folder-box" align="center">
                            <a href="photo-album.php">
                             <img src="images/photo1.jpg" class="img-responsive img-thumbnail img-height" alt="2018" title="2018">
                             <span><strong class="margin-top10">Gallery : 2018</strong><br>
                               3 Albums | Views : 184 views
                             </span>
                             </a>
                        </div>
                                      
                        <div class="col-sm-4 folder-box" align="center">
                            <a href="photo-album.php">
                              <img src="images/photo2.jpg" class="img-responsive img-thumbnail img-height" alt="VICTORY DAY 2017" title="VICTORY DAY 2017">                               
                              <span><strong class="margin-top10">Gallery : VICTORY DAY 2017</strong><br>
                              1 Albums | Views : 65 views
                              </span>
                            </a>
                        </div>
                                      
                        <div class="col-sm-4 folder-box" align="center">
                            <a href="photo-album.php">
                            <img src="images/photo3.jpg" class="img-responsive img-thumbnail img-height" alt="IT CARNIVAL" title="IT CARNIVAL"><span><strong class="margin-top10">Gallery : IT CARNIVAL</strong><br>
                            1 Albums | Views : 480 views
                           </span>
                           </a>
                        </div>

                        <div class="col-sm-4 folder-box" align="center">
                            <a href="photo-album.php">
                            <img src="images/photo4.jpg" class="img-responsive img-thumbnail img-height" alt="Green Dhaka Campaign" title="Green Dhaka Campaign">                                
                            <span><strong class="margin-top10">Gallery : Green Dhaka Campaign</strong><br>1 Albums | Views : 444 views</span>
                            </a>
                        </div>
                                      
                        <div class="col-sm-4 folder-box" align="center">
                           <a href="photo-album.php">
                           <img src="images/photo5.jpeg" class="img-responsive img-thumbnail img-height" alt="Cultural Week-2017" title="Cultural Week-2017">                                
                           <span><strong class="margin-top10">Gallery : Cultural Week-2017</strong><br> 1 Albums | Views : 344 views
                           </span>
                           </a>
                        </div>
                                      
                        <div class="col-sm-4 folder-box" align="center">
                            <a href="photo-album.php">
                            <img src="images/photo5.jpeg" class="img-responsive img-thumbnail img-height" alt="Sport-2017" title="Sport-2017">
                            <span><strong class="margin-top10">Gallery : Sport-2017</strong><br>
                            1 Albums | Views : 373 views
                             </span>
                             </a>
                        </div>
                    </div>
                </div>
            </div>

        <div class="row margin-top10">
            <div class="col-sm-12 col-md-4">
                <ul class="pagination">
                    <li><a href="#"> &laquo; </a></li>
                    <li class="active"><a href="#"> 1 </a></li>
                    <li><a href="#"> 2 </a></li>
                    <li><a href="#"> 3 </a></li>
                    <li><a href="#"> 4 </a></li>
                    <li class="disabled"><a href="#"> &raquo; </a></li>
                </ul>
            </div>
        </div>
                
    </div>
            <!-- Right Side -->
    <div class="col-sm-4 details-right-panel">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="row">
                    <div class="bs-breadcrumb">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home" aria-hidden="true" style="color: white;font-size: 20px"></i></a></li>
                            <li><a href="#">Gallery</a></li>
                            <li><a href="#">Photo Gallery</a></li>
                        </ul>
                    </div>
                </div>

                <div class="row related-topics box-title margin-top10">
                    <a href="video-gallery.php"><img class="img-responsive" src="images/videogallery.jpg" alt="video Gallery"></a>
                 </div>

                <div class="row">
                    <div class="well well-heading"><i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video</div>
                </div>
                <div class="row text-center margin-top10">
                      <iframe width="360" height="200" src="//www.youtube.com/embed/qEF1odHJgKI" frameborder="0" allowfullscreen>
                      </iframe>
                </div>
            </div>
        </div>              
    </div>
</div>
</div>



<?php include("inc/footer.php");?>