<?php include('inc/header.php');?>

<section>
	<div class="container" style="margin-top: 10px;margin-bottom: 10px;">
		<div class="row">
			<div class="col-sm-8">
	            <div class="row">
	            	<div class="col-sm-12">
	            		<div class="panel-group">
				            <div class="panel panel-primary">
				              <div class="panel-heading text-center p-head"><h4><strong>Food & Nutrition Department</strong></h4></div>
				              <div class="panel-body">
				                <p>বাংলাদেশ গার্হস্থ্য অর্থনীতি কলেজ বর্তমান আর্থসামাজিক প্রেক্ষাপটে মেয়েদেও উচ্চ শিক্ষার প্রসারের ক্ষেত্রে গার্হস্থ্য অর্থনীতি শিক্ষার গুরুত্ব বিবেচনায় রেখে বাংলাদেশ গার্হস্থ্য অর্থনীতি কলেজটি প্রতিষ্ঠিত হয়। ঢাকা বিশ্ববিদ্যালয়ে উপাদানকল্প[১] এই কলেজটি বেসরকারি কিন্ত ব্যক্তি মালিকানাধীন নয়। কলেজটি সরকারী অনুদান প্রাপ্ত এম,পি.ও. ভুক্ত। ঢাকা বিশ্ববিদ্যালয় কর্তৃক গঠিত গভর্নিং বডি দ্বারা কলেজটি পরিচালিত।</p>

				                <h4>কোর্স</h4>
				                <p>কলেজটি গার্হস্থ্য অর্থনীতিতে বি.এসসি (সম্মান) ৪ বছর মেয়াদী, বি.এসসি (পাস) ৩ বছর মেয়াদী, এম.এসসি (প্রিলি) ১ বছর মেয়াদী ও এম.এস (ফাইনাল) ১ বছর মেয়াদী কোর্স রয়েছে।[২][৩] এই কলেজের ছাত্রীদের ঢাকা বিশ্ববিদ্যালয় কর্তৃক সনদপত্র প্রদান করা হয়। এবং দরিদ্র মেধাবী শিক্ষার্থীদের মাঝে হারুন কাদেও মোঃ ইউসুফ বৃত্তি প্রদান করা হয়। ঢাকা বিশ্ববিদ্যালয়ের জীববিজ্ঞান অনুষদের সার্বিক তত্ত্বাবধানে সিলেবাস প্রণয়ন করা হয় এবং শিক্ষা কার্যক্রম পরিচালিত হয়। বার্ষিক পরীক্ষাগুলো কার্জন হল এ অনুষ্ঠিত হয়।</p>

				                <h4>বিভাগ</h4>
				                <p>পাঁচটি বিভাগ হলঃ</p><br>
				                <p>->গৃহ ব্যবস্থাপনা ও গৃহায়ন</p><br>
				                <p>->শিশু বিকাশ ও সামাজিক সম্পর্ক</p><br>
				                <p>->ব্যবহারিক শিল্পকলা</p><br>
				                <p>->বস্ত্র পরিচ্ছদ ও বয়ন শিল্প</p>

				                <h4>ভর্তি</h4>
				                <p>ঢাকা বিশ্ববিদ্যালয়ের গার্হস্থ্য অর্থনীতি ইউনিট এ ভর্তি পরীক্ষা দিয়ে এই কলেজে ভর্তি হওয়া যায়। আসন সংখ্যা ৫৫০ টি</p>

				                <h4>ক্যাম্পাস</h4>
				                <p>৬ষ্ঠ তলা ভবনে কলেজটি ১৪৬/৪ গ্রীনরোড,(পানি উন্নয়ন বোর্ডের অপর দিকে) এ অবস্থিত।</p>
				              </div>
				            </div>
				          </div>
	            	</div>
	            </div>

	            <div class="row mp" style="margin-top: 5px; color: #736AFF;font-weight: bold;">
      				<div class="col-md-12 box">
        				Total Views : 2369 views
      				</div>
    			</div>
	</div>
			<div class="col-sm-4 details-right-panel">
	          <div class="row mp">
	              <div class="col-xs-12 col-sm-12">
	                <div class="row">
	                    <div class="bs-breadcrumb">
						    <ul class="breadcrumb">
						        <li><a href="#"><i class="fa fa-home" aria-hidden="true" style="color: white;font-size: 20px"></i></a></li>
						        <li><a href="#">Department</a></li>
						        <li><a href="#">Food & Nutrition</a></li>
						    </ul>
						</div>
	                </div>
	                  <div class="row">
	                        <div class="well well-heading"><i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video</div>
	                  </div>
	                  <div class="row text-center margin-top10">
	                      <iframe width="360" height="200" src="//www.youtube.com/embed/qEF1odHJgKI" frameborder="0" allowfullscreen>
	                      </iframe>
	                  </div>

	                  <div class="row">
	                      <div class="well well-heading"><i class="fa fa-windows" aria-hidden="true"></i> Related Topics</div>
	                  </div>
	                  <div class="row margin-bottom20">
	                      	<ul class="list-unstyled">
	                          <li class="list-group-item"><a href="child-development.php"><i class="fa fa-history"></i> Child Development & Social Relationship</a></li>
	                          <li class="list-group-item"><a href="resource-management.php"><i class="fa fa-newspaper-o"></i> Resource Management & Entrepreneurship</a></li>
	                          <li class="list-group-item"><a href="arts-creative.php"><i class="fa fa-university"></i> Art & Creative Studies</a></li></li>
	                          <li class="list-group-item"><a href="clothing-textile.php"><i class="fa fa-newspaper-o"></i> Clothing & Textile</a></li>  
	                      	</ul>                   
	              		</div>
	          		</div>
	        	</div>              
    		</div>
	</div>
</section>



<?php include('inc/footer.php');?>