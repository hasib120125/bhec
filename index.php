<?php require_once("db/connection.php");require_once("db/config.php");?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title >Bangladesh Home Economics Collage 1996</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
  </head>

  <body>

  <!--Header Section Start-->
      <?php include_once('inc/header.php');?>
  <!--Header Section End-->

    <!--Content Area Start-->
  <div class="container">
         <div class="row" style="margin-top: 5px">
            <div class="col-sm-12" style="margin-bottom: 5px;">
                <div class="breakingNews bn-turquoise" id="bn3">
                    <div class="bn-title" style="width: auto;"><h2 style="display: inline-block;">Latest Notices : </h2><span></span>
                    </div>
                    <ul style="left: 160px;">
                        <li style="display: block;">
                            <h4 style="margin-left: 20px; color: red;" class="blink_me">
                            <marquee behavior="" direction="" onmouseover="this.stop();" onmouseout="this.start();">
                            <p>
                            <strong style="padding:0px 10px;color: green !important; "><a style="font-size:75% !important;" href="scroll-notice-details?noticeid=26" target="_blank" class="scroll-notice-link"> নিয়োগ বিজ্ঞপ্তি ||</a>
                            </strong>

                            <strong style="padding:0px 10px;color: green !important; "><a style="font-size:75% !important;" href="scroll-notice-details?noticeid=7" target="_blank" class="scroll-notice-link"> অনলাইনে টিউশন ফি প্রদানের নিয়মাবলি  ||</a>
                            </strong>
                            </p>
                            </marquee>
                             </h4>
                         </li>
                    </ul>
                </div>
            </div>
             
        </div>
        <div class="row"> <!--Slider Section Start-->
          <div class="col-sm-12">
               <div id="first-slider">
                  <div id="carousel-example-generic" class="carousel slide carousel-fade">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                      </ol>
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">
                          <!-- Item 1 -->
                          <div class="item active slide1">
                              <div class="row"><div class="container">
                                  <div class="col-md-3 text-right">
                                      <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="images/slider1.jpg">
                                  </div>
                                  <div class="col-md-9 text-left">
                                      <h3 data-animation="animated bounceInDown">Add images, or even your logo!</h3>
                                      <h4 data-animation="animated bounceInUp">Easily use stunning effects</h4>             
                                   </div>
                              </div></div>
                           </div> 
                          <!-- Item 2 -->
                          <div class="item slide2">
                              <div class="row"><div class="container">
                                  <div class="col-md-7 text-left">
                                      <h3 data-animation="animated bounceInDown"> 50 animation options A beautiful</h3>
                                      <h4 data-animation="animated bounceInUp">Create beautiful slideshows </h4>
                                   </div>
                                  <div class="col-md-5 text-right">
                                      <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="images/slider2.jpg">
                                  </div>
                              </div></div>
                          </div>
                          <!-- Item 3 -->
                          <div class="item slide3">
                              <div class="row"><div class="container">
                                  <div class="col-md-7 text-left">
                                      <h3 data-animation="animated bounceInDown">Simple Bootstrap Carousel</h3>
                                      <h4 data-animation="animated bounceInUp">Bootstrap Image Carousel Slider with Animate.css</h4>
                                   </div>
                                  <div class="col-md-5 text-right">
                                      <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="images/slider3.jpg">
                                  </div>     
                              </div></div>
                          </div>
                          <!-- Item 4 -->
                          <div class="item slide4">
                              <div class="row"><div class="container">
                                  <div class="col-md-7 text-left">
                                      <h3 data-animation="animated bounceInDown">We are creative</h3>
                                      <h4 data-animation="animated bounceInUp">Get start your next awesome project</h4>
                                   </div>
                                  <div class="col-md-5 text-right">
                                      <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="images/slider4.jpg">
                                  </div>  
                              </div></div>
                          </div>
                          <!-- End Item 4 -->

                      </div>
                       <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                          <i class="fa fa-angle-left"></i><span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                          <i class="fa fa-angle-right"></i><span class="sr-only">Next</span>
                        </a>

                    </div>
              </div>
        </div>
      </div>  <!--Slider Section End-->

      <!--Main Body  Section Start-->
      <div class="row">
        <div class="col-sm-8 welcome_message"><!--Body Left Section Start-->
          <div class="panel-group"> <!--Body Left first Section Start-->
            <div class="panel panel-primary">
              <div class="panel-heading text-center"><h4><strong>বাংলাদেশ গার্হস্থ্য অর্থনীতি কলেজ</strong></h4></div>
              <div class="panel-body" style="font-size: 15px;text-align: justify;">বর্তমান আর্থসামাজিক প্রেক্ষাপটে মেয়েদের উচ্চ শিক্ষার প্রসারে কর্মক্ষেত্র এবং বাস্তব জীবনে ছাত্রীদের উপযোগী করে গড়ে তোলার লক্ষে ১৯৯৬ সালে বাংলাদেশ গার্হস্থ্য অর্থনীতি কলেজটি প্রতিষ্ঠিত হয় । ঢাকা বিশ্ববিদ্যালয়ের জীববিজ্ঞান অনুষদের সার্বিক তত্ত্বাবধানে, অনুমোদিত সিলেবাস সহ সকল কার্যক্রম পরিচালিত হয় । নিয়মিত ক্লাস লেকচার, পরীক্ষা মনিটরিং, পর্যালোচনা দুর্বল ছাত্রীদের পৃথক ক্লাসের ব্যবস্থার পাশাপাশি বার্ষিক পরিক্ষায় ভাল ফলাফলের জন্য বৃত্তি প্রদানের ব্যবস্থা রয়েছে । ইতিমধ্যে এই প্রতিষ্ঠান থেকে বিভিন্ন ব্যাচ সফলতার সঙ্গে বি.এসসি (সম্মান)এবং মাস্টার্স কোর্স সম্পন্ন করে বিভিন্ন প্রতিষ্ঠানে যোগ্যতা ও সুনামের সঙ্গে কর্মরত আছে এবং বর্তমানে এই কলেজের ২১তম ব্যাচের ভর্তি প্রক্রিয়া চলছে । কলেজটিতে গার্হস্থ্য অর্থনীতি বিষয়ে নিম্নোক্ত কোর্স /ডিগ্রীসমূহ প্রদান করা হয় ।

              <h4>কোর্সের নাম :</h4>
              <span><p>১. বি.এসসি (সম্মান) ৪ বৎসর মেয়াদী</p></span>
              <span><p>১২. বি.এসসি (পাস) ৩ বৎসর মেয়াদী</p></span>
              <span><p>১৩. এম.এসসি (প্রিলিমিনারী) ১ বৎসর মেয়াদী</p></span>
              <span><p>১৪. এম.এস/এম.এসসি (ফাইনাল) ১ বৎসর মেয়াদী</p></span>

              <h4>কোর্স সমূহ:</h4>
              <span><p>* খাদ্য ও পুষ্টি বিজ্ঞান</p></span>
              <span><p>* রিসোর্স ম্যানেজমেণ্ট ও এন্টারপ্রেনারশিপ</p></span>
              <span><p>* শিশু বিকাশ ও সামাজিক সম্পর্ক</p></span>
              <span><p>* শিল্প কলা ও সৃজনশীল শিক্ষা</p></span>
              <span><p>* বস্ত্র পরিচ্ছদ ও বয়নশিল্প</p></span>
            </div>
            </div>
          </div><!--Body Left First Section End-->

          <!--Body Left Second Section Start-->
          <div class="row">
             <!--Achievements Section Start-->
            <div class="col-sm-6">
              <div class="panel-group">
                <div class="panel panel-primary">
                  <div class="panel-heading text-center"><h4><i class="fa fa-trophy" aria-hidden="true">Achievements</i></h4></div>
                  <div class="panel-body">
                    <div class="row bottom-border">
                      <div class="col-sm-12">
                        <a href="">
                         <div class="row thumblink border-bottom">
                           <div class="col-xs-3 col-sm-3 padding-unset"><img src="images/sample1.png" class="img-responsive" alt="" title="The Best School in Dhaka Mohanagar in National Education Week-2017."></div>
                           <div class="col-xs-9 col-sm-9 border-left"><span>The Best School in Dhaka Mohanagar in National Education Week-2017.</span><br>
                            <span>23 Mar, 2017</span>
                          </div>
                          </div> 
                        </a>
                    </div>
                  </div>

                  <div class="row bottom-border">
                      <div class="col-sm-12">
                        <a href="">
                         <div class="row thumblink border-bottom">
                           <div class="col-xs-3 col-sm-3 padding-unset"><img src="images/sample1.png" class="img-responsive" alt="" title="The Best School in Dhaka Mohanagar in National Education Week-2017."></div>
                           <div class="col-xs-9 col-sm-9 border-left"><span>The Best School in Dhaka Mohanagar in National Education Week-2017.</span><br>
                            <span>23 Mar, 2017</span>
                          </div>
                          </div> 
                        </a>
                    </div>
                  </div>

                  <div class="row bottom-border">
                      <div class="col-sm-12">
                        <a href="">
                         <div class="row thumblink border-bottom">
                           <div class="col-xs-3 col-sm-3 padding-unset"><img src="images/sample1.png" class="img-responsive" alt="" title="The Best School in Dhaka Mohanagar in National Education Week-2017."></div>
                           <div class="col-xs-9 col-sm-9 border-left"><span>The Best School in Dhaka Mohanagar in National Education Week-2017.</span><br>
                            <span>23 Mar, 2017</span>
                          </div>
                          </div> 
                        </a>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12">
                        <a href="">
                         <div class="row thumblink border-bottom">
                           <div class="col-xs-3 col-sm-3 padding-unset"><img src="images/sample1.png" class="img-responsive" alt="" title="The Best School in Dhaka Mohanagar in National Education Week-2017."></div>
                           <div class="col-xs-9 col-sm-9 border-left"><span>The Best School in Dhaka Mohanagar in National Education Week-2017.</span><br>
                            <span>23 Mar, 2017</span>
                          </div>
                          </div> 
                        </a>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>  <!--Achievements Section End-->

          <!--Events Section Start-->
          <div class="col-sm-6">
              <div class="panel-group">
                <div class="panel panel-primary">
                  <div class="panel-heading text-center"><h4><i class="fa fa-calendar" aria-hidden="true">&nbsp;&nbsp;News & Events</i></h4></div>
                  <div class="panel-body">
                    <div class="row bottom-border">
                      <div class="col-sm-12">
                        <a href="">
                         <div class="row thumblink border-bottom">
                           <div class="col-xs-3 col-sm-3 padding-unset"><img src="images/sample1.png" class="img-responsive" alt="" title="The Best School in Dhaka Mohanagar in National Education Week-2017."></div>
                           <div class="col-xs-9 col-sm-9 border-left"><span>The Best School in Dhaka Mohanagar in National Education Week-2017.</span><br>
                            <span>23 Mar, 2017</span>
                          </div>
                          </div> 
                        </a>
                    </div>
                  </div>

                  <div class="row bottom-border">
                      <div class="col-sm-12">
                        <a href="">
                         <div class="row thumblink border-bottom">
                           <div class="col-xs-3 col-sm-3 padding-unset"><img src="images/sample1.png" class="img-responsive" alt="" title="The Best School in Dhaka Mohanagar in National Education Week-2017."></div>
                           <div class="col-xs-9 col-sm-9 border-left"><span>The Best School in Dhaka Mohanagar in National Education Week-2017.</span><br>
                            <span>23 Mar, 2017</span>
                          </div>
                          </div> 
                        </a>
                    </div>
                  </div>

                  <div class="row bottom-border">
                      <div class="col-sm-12">
                        <a href="">
                         <div class="row thumblink border-bottom">
                           <div class="col-xs-3 col-sm-3 padding-unset"><img src="images/sample1.png" class="img-responsive" alt="" title="The Best School in Dhaka Mohanagar in National Education Week-2017."></div>
                           <div class="col-xs-9 col-sm-9 border-left"><span>The Best School in Dhaka Mohanagar in National Education Week-2017.</span><br>
                            <span>23 Mar, 2017</span>
                          </div>
                          </div> 
                        </a>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12">
                        <a href="">
                         <div class="row thumblink border-bottom">
                           <div class="col-xs-3 col-sm-3 padding-unset"><img src="images/sample1.png" class="img-responsive" alt="" title="The Best School in Dhaka Mohanagar in National Education Week-2017."></div>
                           <div class="col-xs-9 col-sm-9 border-left"><span>The Best School in Dhaka Mohanagar in National Education Week-2017.</span><br>
                            <span>23 Mar, 2017</span>
                          </div>
                          </div> 
                        </a>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div> <!--Events Section End-->

        </div><!--Body Left Second Section End-->

        <!--Body Left Third Section Start-->
          <div class="row">
            <div class="col-sm-6">
              <iframe width="360" height="200" src="//www.youtube.com/embed/qEF1odHJgKI" frameborder="0" allowfullscreen>
              </iframe>
            </div>

            <div class="col-sm-6">
                <div fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=&amp;container_width=340&amp;height=430&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Facps1960%2F&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;show_posts=false&amp;small_header=false&amp;width=340" fb-xfbml-state="rendered" class="fb-page fb_iframe_widget" data-href="https://www.facebook.com/acps1960/" data-width="340" data-height="430" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><span style="vertical-align: bottom; width: 360px; height: 200px;"><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fhecollege%2F&tabs=timeline&width=340&height=205&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="205" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></span></div>
            </div>
          </div>
        <!--Body Left Third Section End-->

      </div><!--Body Left Section End-->

      <!--Body Right Section Start-->
        <div class="col-sm-4 messages"> 
        <!--Notice Section Start-->
          <div class="panel-group" style="margin-bottom: 10px;">
            <div class="panel panel-primary">
              <div class="panel-heading text-center"><h4><i class="fa fa-bullhorn" aria-hidden="true">&nbsp;&nbsp;Notice</i></h4></div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-sm-12">
                  <marquee scrollamount="2" onmouseover="this.setAttribute('scrollamount', 4, 0);" onmouseout="this.setAttribute('scrollamount', 4, 0);" direction="up">
                    <div class="row notice content-border">
                      <div class="col-sm-2 col-xs-2 font-bold notice-sidebar"><span> 8 </span><br><p>Mar</p> </div>
                      <div class="col-sm-2 col-xs-2 year">2018</div>
                      <div class="col-sm-8 col-xs-8 notice-details"><a href="notice-details?nid=84" target="_blank">Monthly Tuition and other fees have to be paid through online banking system from 1 April 2018.</a></div>
                    </div>

                    <div class="row notice content-border">
                      <div class="col-sm-2 col-xs-2 font-bold notice-sidebar"><span> 8 </span><br><p>Mar</p></div>
                      <div class="col-sm-2 col-xs-2 year">2018</div>
                      <div class="col-sm-8 col-xs-8 notice-details"><a href="notice-details?nid=84" target="_blank">Monthly Tuition and other fees have to be paid through online banking system from 1 April 2018.</a></div>
                    </div>

                    <div class="row notice content-border">
                      <div class="col-sm-2 col-xs-2 font-bold notice-sidebar"><span> 8 </span><br><p>Mar</p></div>
                      <div class="col-sm-2 col-xs-2 year">2018</div>
                      <div class="col-sm-8 col-xs-8 notice-details"><a href="notice-details?nid=84" target="_blank">Monthly Tuition and other fees have to be paid through online banking system from 1 April 2018.</a></div>
                    </div>

                    <div class="row notice">
                      <div class="col-sm-2 col-xs-2 font-bold notice-sidebar"><span> 8 </span><br><p>Mar</p></div>
                      <div class="col-sm-2 col-xs-2 year">2018</div>
                      <div class="col-sm-8 col-xs-8 notice-details"><a href="notice-details?nid=84" target="_blank">Monthly Tuition and other fees have to be paid through online banking system from 1 April 2018.</a></div>
                    </div>
                    </marquee>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--Notice Section End-->

          <!--Authirity Messages Section Start-->
          <div class="panel-group" style="margin-bottom: 10px;">
            <div class="panel panel-primary">
              <div class="panel-heading text-center"><h4><i class="fa fa-envelope-o" aria-hidden="true">&nbsp;&nbsp;Authirity Messages</i></h4></div>
              <div class="panel-body">
                <div class="row bottom-border">
                  <div class="col-sm-12 authirity-message">
                     <a href="message-chairman.php"><div class="col-sm-4"><img src="images/chairman.jpeg" class="img-responsive  img-thumbnail" alt="অধ্যাপক মো: আনোয়ার হোসেন" title="Brig Gen Iqbal Ahmed, ndc, afwc, psc"></div>
                      <div class="col-sm-8 mp"><span style="font-weight:bold;">Message From Chairman<br>অধ্যাপক মো: আনোয়ার হোসেন</span><br>অধ্যাপক, প্রাণ রসায়ন ও অনুপ্রাণ বিজ্ঞান বিভাগ ঢাকা বিশ্ববিদ্যালয়, ঢাকা।</div></a>
                  </div>
                </div>
                
                <div class="row bottom-border">
                  <div class="col-sm-12 authirity-message">
                     <a href="message-principle"><div class="col-sm-4"><img src="images/chairman.jpeg" class="img-responsive  img-thumbnail" alt="Brig Gen Iqbal Ahmed, ndc, afwc, psc" title="Brig Gen Iqbal Ahmed, ndc, afwc, psc"></div>
                      <div class="col-sm-8 mp"><span style="font-weight:bold;">Message From Principle<br>Brig Gen Iqbal Ahmed, ndc, afwc, psc</span><br>Principle</div></a>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12 authirity-message">
                     <a href="message-vice-principle"><div class="col-sm-4"><img src="images/chairman.jpeg" class="img-responsive  img-thumbnail" alt="Brig Gen Iqbal Ahmed, ndc, afwc, psc" title="Brig Gen Iqbal Ahmed, ndc, afwc, psc"></div>
                      <div class="col-sm-8 mp"><span style="font-weight:bold;">Message From Vice-Principle<br>Brig Gen Iqbal Ahmed, ndc, afwc, psc</span><br>Vice-Principle</div></a>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <!--Authirity Messages Section End-->
          
          <!--Quick Link Section Start-->
          <div class="panel-group">
            <div class="panel panel-primary">
              <div class="panel-heading text-center"><h4><i class="fa fa-external-link" aria-hidden="true">&nbsp;&nbsp;Quick Links</i></h4></div>
              <div class="panel-body" style="font-size: 18px;">
                <div class="row content-border">
                  <div class="col-sm-12">
                    <a href="" target="_blank"><i class="fa fa-university" aria-hidden="true">&nbsp;&nbsp;Dhaka University Result</i></a>
                  </div>
                </div>

                <div class="row content-border">
                  <div class="col-sm-12">
                    <a href="" target="_blank"><i class="fa fa-university" aria-hidden="true">&nbsp;&nbsp;Dhaka University Info</i></a>
                  </div>
                </div>

                <div class="row content-border">
                  <div class="col-sm-12">
                    <a href="" target="_blank"><i class="fa fa-university" aria-hidden="true">&nbsp;&nbsp;NU Result</i></a>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <a href="" target="_blank"><i class="fa fa-university" aria-hidden="true">&nbsp;&nbsp;All University Result</i></a>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <!--Quick Link Section End-->

          <!--Visitor Statistics Section Start-->
          <div class="panel-group">
            <div class="panel panel-primary">
              <div class="panel-heading text-center"><h4><strong><i class="fa fa-line-chart" aria-hidden="true">&nbsp;&nbsp;Visitor Statistics</i></strong></h4></div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-sm-6">
                    <ul class="list-unstyled">
                      <li class="list-group-item-success">5 Online</li>
                      <li class="list-group-item-danger">122 Today</li>
                      <li class="list-group-item-warning">438 Yesterday</li>
                    </ul>
                  </div>
                  <div class="col-sm-6">
                    <ul class="list-unstyled">
                      <li class="list-group-item-success">980 Week</li>
                      <li class="list-group-item-danger">7910 Month</li>
                      <li class="list-group-item-warning">30025 Year</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--Visitor Statistics Section End-->
        </div>
        <!--Body Right Section End-->
      </div>
      <!--Main Body  Section End-->

  </div>
    <!--Content Area End-->

    <!--Footer Section Start-->
      
         <?php include_once('inc/footer.php');?>

    <!--Header Section End-->
     

  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

  <script type="text/javascript">
    (function( $ ) {

          //Function to animate slider captions 
        function doAnimations( elems ) {
          //Cache the animationend event in a variable
          var animEndEv = 'webkitAnimationEnd animationend';
          
          elems.each(function () {
            var $this = $(this),
              $animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function () {
              $this.removeClass($animationType);
            });
          });
        }
        
        //Variables on page load 
        var $myCarousel = $('#carousel-example-generic'),
          $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
          
        //Initialize carousel 
        $myCarousel.carousel();
        
        //Animate captions in first slide on page load 
        doAnimations($firstAnimatingElems);
        
        //Pause carousel  
        $myCarousel.carousel('pause');
        
        
        //Other slides to be animated on carousel slide event 
        $myCarousel.on('slide.bs.carousel', function (e) {
          var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
          doAnimations($animatingElems);
        });  
          $('#carousel-example-generic').carousel({
              interval:1000,
              pause: "false"
          });
        
      })(jQuery); 
  </script>
</html>
