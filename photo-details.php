<?php include("inc/header.php");?>


<div class="container">
     <div class="row margin-top10 mp">
         <div class="col-sm-8 details-left-panel">
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading p-head">Album name - Sample</div>
                    <div class="panel-body">             
                        <div class="row">
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 1" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350"></a></div>
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 2" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350/2255EE"></a></div>
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 3" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350/449955/FFF"></a></div>
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 4" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350/992233"></a></div>
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 5" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350/2255EE"></a></div>
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 6" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350/449955/FFF"></a></div>
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 8" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350/777"></a></div>
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 9" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350/992233"></a></div>
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 10" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350/EEE"></a></div>
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 11" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350/449955/FFF"></a></div>
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 12" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350/DDD"></a></div>
                          <div class="col-lg-3 col-sm-4 col-xs-6"><a title="Image 13" href="#"><img class="thumbnail img-responsive" src="//placehold.it/600x350/992233"></a></div>
                        </div>
                    </div>
                    <div class="modal" id="myModal" role="dialog">
                      <div class="modal-dialog">
                      <div class="modal-content">
                      <div class="modal-header">
                        <button class="close" type="button" data-dismiss="modal">×</button>
                        <h3 class="modal-title"></h3>
                      </div>
                      <div class="modal-body">
                        <div id="modalCarousel" class="carousel">
                              <div class="carousel-inner"></div>
                              <a class="carousel-control left" href="#modaCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                              <a class="carousel-control right" href="#modalCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                            </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                       </div>
                      </div>      
                    </div>
                </div>
            </div>

        <div class="row margin-top10">
            <div class="col-sm-12 col-md-4">
                <ul class="pagination">
                    <li><a href="#"> &laquo; </a></li>
                    <li class="active"><a href="#"> 1 </a></li>
                    <li><a href="#"> 2 </a></li>
                    <li><a href="#"> 3 </a></li>
                    <li><a href="#"> 4 </a></li>
                    <li class="disabled"><a href="#"> &raquo; </a></li>
                </ul>
            </div>
        </div>
                
    </div>
            <!-- Right Side -->
    <div class="col-sm-4 details-right-panel">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="row">
                    <div class="bs-breadcrumb">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Gallery</a></li>
                            <li><a href="#">Photo Details</a></li>
                        </ul>
                    </div>
                </div>

                <div class="row related-topics box-title margin-top10">
                    <a href="video-gallery.php"><img class="img-responsive" src="images/videogallery.jpg" alt="video Gallery"></a>
                 </div>

                <div class="row">
                    <div class="well well-heading"><i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video</div>
                </div>
                <div class="row text-center margin-top10">
                      <iframe width="360" height="200" src="//www.youtube.com/embed/qEF1odHJgKI" frameborder="0" allowfullscreen>
                      </iframe>
                </div>
            </div>
        </div>              
    </div>
</div>
</div>





<?php include("inc/footer.php");?>

<script type="text/javascript" src="js/photo-details.js"></script>