<?php include('inc/header.php');?>

<div class="container" style="margin-top: 10px;margin-bottom: 10px;">
	<div class="col-md-8">
    	<div class="row mp">
            <div class="col-md-12 box mar-bottom10">
            	<div class="col-sm-5">
                    <img src="https://www.dcc.edu.bd/media/imgAll/20-09-2016-1474359930.jpg" class="img-responsive" alt="Discussion Against Extremism and Terrorism" title="Discussion Against Extremism and Terrorism">
                </div>
                <div class="col-sm-7">
                	<h4 class="txt-color">Discussion Against Extremism and Terrorism</h4>
                    <p>Dhaka Commerce College organised a discussion programme at Professor Kazi Nurul Islam Faruky Auditorium on 3 September 2016 with a view to raising national awareness against extremism and terrorism
                	</p>
                    <div class="event-bottom">
                        <a href="#" style="color:coral;font-weight:bold;"><button type="button" class="btn btn-primary">Read More</button></a>
                        <span>Total Views : 939</span>
                    </div>
                </div>
            </div>
            <div class="col-md-12 box mar-bottom10">
            	<div class="col-sm-5">
                    <img src="https://www.dcc.edu.bd/media/imgAll/20-09-2016-1474359812.jpg" class="img-responsive" alt="River Cruise 2016" title="River Cruise 2016">
                </div>
                <div class="col-sm-7">
                	<h4 class="txt-color">River Cruise 2016</h4>
                    <p>The annual River cruise of Dhaka Commerce College was held on 2 September 2016. Sudarban10, one of the largest launches in the country, was selected as the vehicle. More than 15 hundred people part
                	</p>
                    <div class="event-bottom">
                        <a href="#" style="color:coral;font-weight:bold;"><button type="button" class="btn btn-primary">Read More</button></a>
                        <span>Total Views : 939</span>
                    </div>
                </div>
            </div>
            <div class="col-md-12 box mar-bottom10">
            	<div class="col-sm-5">
                    <img src="https://www.dcc.edu.bd/media/imgAll/20-09-2016-1474359701.jpg" class="img-responsive" alt="Human Chain on Behalf of Local MP" title="Human Chain on Behalf of Local MP">
                </div>
                <div class="col-sm-7">
                	<h4 class="txt-color">Human Chain on Behalf of Local MP</h4>
                    <p>In accordance with the will of the local MP Aslamul Haque students, teachers and staffs of Dhaka Commerce College attended at a Human Chain Program against extremism, terrorism and militancy at the
                	</p>
                    <div class="event-bottom">
                        <a href="#" style="color:coral;font-weight:bold;"><button type="button" class="btn btn-primary">Read More</button></a>
                        <span>Total Views : 939</span>
                    </div>
                </div>
            </div>
            <div class="col-md-12 box mar-bottom10">
            	<div class="col-sm-5">
                    <img src="https://www.dcc.edu.bd/media/imgAll/20-08-2016-1471693341.jpg" class="img-responsive" alt="National Mourning Day 2016 Observed" title="National Mourning Day 2016 Observed">
                </div>
                <div class="col-sm-7">
                	<h4 class="txt-color">National Mourning Day 2016 Observed</h4>
                    <p>The National Mourning Day 2016 marking the 41st anniversary of the assassination of the Father of the Nation Bangaboandhu Sheikh Mujibur Rahman was observed in Dhaka Commerce College on 17 August 2
                	</p>
                    <div class="event-bottom">
                        <a href="#" style="color:coral;font-weight:bold;"><button type="button" class="btn btn-primary">Read More</button></a>
                        <span>Total Views : 939</span>
                    </div>
                </div>
            </div>
            <div class="col-md-12 box mar-bottom10">
            	<div class="col-sm-5">
                    <img src="https://www.dcc.edu.bd/media/imgAll/02-08-2016-1470136785.jpg" class="img-responsive" alt="Human Chain" title="Human Chain">
                </div>
                <div class="col-sm-7">
                	<h4 class="txt-color">Human Chain</h4>
                    <p>Dhaka Commerce College organised a human chain (manob bondon) program against terrorism and militancy on 1 August 2016 in front of the college. Nearly 6 thousand students, teachers and staffs parti
                	</p>
                    <div class="event-bottom">
                        <a href="#" style="color:coral;font-weight:bold;"><button type="button" class="btn btn-primary">Read More</button></a>
                        <span>Total Views : 939</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row margin-top10">
            <div class="col-sm-12 col-md-4">
              <ul class="pagination">
                  <li><a href="#"> &laquo; </a></li>
                  <li class="active"><a href="#"> 1 </a></li>
                  <li><a href="#"> 2 </a></li>
                  <li><a href="#"> 3 </a></li>
                  <li><a href="#"> 4 </a></li>
                  <li class="disabled"><a href="#"> &raquo; </a></li>
              </ul>
            </div>
        </div>
        	           
    </div>

    <div class="col-sm-4 details-right-panel">
      <div class="row">
          <div class="col-xs-12 col-sm-12">
              <div class="row">
                    <div class="bs-breadcrumb">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home" aria-hidden="true" style="color: white;font-size: 20px"></i></a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Achievement</a></li>
                        </ul>
                    </div>
                </div>
              <div class="row">
                  <div class="well well-heading"><i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video</div>
              </div>
              <div class="row text-center margin-top10">
                  <iframe width="360" height="200" src="//www.youtube.com/embed/qEF1odHJgKI" frameborder="0" allowfullscreen>
                  </iframe>
              </div>

                <div class="row">
                    <div class="well well-heading"><i class="fa fa-windows" aria-hidden="true"></i> Related Topics</div>
                </div>
              <div class="row margin-bottom20">
                    <ul class="list-unstyled">
                      <li class="list-group-item"><a href="details.php"><i class="fa fa-history"></i> At a Glance</a></li>
                      <li class="list-group-item"><a href="achievement.php"><i class="fa fa-newspaper-o"></i> History</a></li>
                      <li class="list-group-item"><a href="infrastructure.php"><i class="fa fa-university"></i> Infrastructure</a></li></li>
                      <li class="list-group-item"><a href="news-and-events.php"><i class="fa fa-newspaper-o"></i> News & Events</a></li>  
                    </ul>                                      
                </div>
            </div>
        </div>              
    </div>
</div>



<?php include('inc/footer.php');?>