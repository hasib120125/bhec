<?php include('inc/header.php');?>


<div class="container" style="margin-top: 10px;margin-bottom: 10px;">
<div class="row mp">
  <div class="col-sm-8 details-left-panel">
      <div class="row box-title white-bg text-justify">
        <h1 class="details-header">Bangladesh Home Economics Collage Admission Circular</h1>
      </div>

      <div class="row body-container margin-top10 white-bg padding15">
        <div class="row">
          <div class="col-sm-12">
            <h6 class="brief-title">Admission Circular - 2018</h6><br><small>Oct 16, 2017 </small>|<span><small> Total views : <b>61093</b></small></span>
          </div>
        </div>
        <img src="images/bhec_admission18.jpg" class="img-responsive margin-bottom2P" alt="Admission Circular - 2018" title="Admission Circular - 2018">
        <div class="padding15"><br></div>
        <input type="hidden" value="x">
      </div>
      
    
    <div class="row margin-top10">
      <div class="col-sm-12 col-md-4">
        <ul class="pagination">
          <li><a href="#"> &laquo; </a></li>
            <li class="active"><a href="#"> 1 </a></li>
            <li><a href="#"> 2 </a></li>
            <li><a href="#"> 3 </a></li>
            <li><a href="#"> 4 </a></li>
            <li class="disabled"><a href="#"> &raquo; </a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-sm-4 details-right-panel">
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <div class="row">
                    <div class="btn-group btn-breadcrumb">
                        <a href="#" class="btn btn-primary"><i class="glyphicon glyphicon-home"></i></a>
                        <a href="#" class="btn btn-primary">Snippets</a>
                        <a href="#" class="btn btn-primary">Breadcrumbs</a>
                        <a href="#" class="btn btn-primary">Primary</a>
                    </div>
                  </div>
                  <div class="row right-video margin-top10 box-title">
                        <i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video
                  </div>
                  <div class="row text-center margin-top10">
                      <iframe width="360" height="200" src="//www.youtube.com/embed/58-atNakMWw" frameborder="0" allowfullscreen>
                      </iframe>
                  </div>

                  <div class="row related-topics box-title">
                      <i class="fa fa-windows" aria-hidden="true"></i> Related Topics
                  </div>
                  <div class="row margin-bottom20">
                      <ul class="list-unstyled">
                          <li class="list-group-item"><a href="#"><i class="fa fa-calendar" aria-hidden="true"> </i> Prospectus</a></li>
                          <li class="list-group-item"><a href="#"><i class="fa fa-table" aria-hidden="true"> </i> Admission Form</a></li>
                          <li class="list-group-item"><a href="#"><i class="fa fa-file-o" aria-hidden="true"> </i> Admission Result</a></li>
                          <li class="list-group-item"><a href="#"><i class="fa fa-list" aria-hidden="true"> </i> Waiting List</a></li>
                          <li class="list-group-item"><a href="#"><i class="fa fa-columns" aria-hidden="true"> </i> Courses/Programs</a></li>
                          <li class="list-group-item"><a href="#"><i class="fa fa-bell" aria-hidden="true"> </i> Fees</a></li>
                          <li class="list-group-item"><a href="#"><i class="fa fa-bell" aria-hidden="true"> </i> Download Admit Card</a></li>
                      </ul>                   
              </div>
          </div>
        </div>              
    </div>
</div>
</div>


<?php include('inc/footer.php');?>