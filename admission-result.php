<?php include('inc/header.php');?>


<div class="container" style="margin-top: 10px;margin-bottom: 10px;">
<div class="row mp">
  <div class="col-sm-8 details-left-panel">
    <div class="panel-group">
      <div class="panel panel-primary">
        <div class="panel-heading p-head">Bangladesh Home Economics Collage Admission Result</div>
        <div class="panel-body">
          <div class="col-md-12 box mar-top10 admission" align="center">
              <a href="#" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 70px;color: #3C5487;"></i></a><br>
              <a href="#"><h4><i class="fa fa-download" aria-hidden="true"></i> BBA &amp; CSE (Hons.) Professional Admission: 2017-2018 / Total Views : 208 views</h4></a>
          </div>

          <div class="col-md-12 box mar-top10 admission" align="center">
              <a href="#" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 70px;color: #3C5487;"></i></a><br>
              <a href="#"><h4><i class="fa fa-download" aria-hidden="true"></i> BBA &amp; CSE (Hons.) Professional Admission: 2017-2018 / Total Views : 208 views</h4></a>
          </div>

          <div class="col-md-12 box mar-top10 admission" align="center">
              <a href="#" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 70px;color: #3C5487;"></i></a><br>
              <a href="#"><h4><i class="fa fa-download" aria-hidden="true"></i> BBA &amp; CSE (Hons.) Professional Admission: 2017-2018 / Total Views : 208 views</h4></a>
          </div>

          <div class="col-md-12 box mar-top10 admission" align="center">
              <a href="#" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 70px;color: #3C5487;"></i></a><br>
              <a href="#"><h4><i class="fa fa-download" aria-hidden="true"></i> BBA &amp; CSE (Hons.) Professional Admission: 2017-2018 / Total Views : 208 views</h4></a>
          </div>

        </div>
      </div>
    </div>

    <div class="row margin-top10">
      <div class="col-sm-12 col-md-4">
        <ul class="pagination">
          <li><a href="#"> &laquo; </a></li>
            <li class="active"><a href="#"> 1 </a></li>
            <li><a href="#"> 2 </a></li>
            <li><a href="#"> 3 </a></li>
            <li><a href="#"> 4 </a></li>
            <li class="disabled"><a href="#"> &raquo; </a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-sm-4 details-right-panel">
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <div class="row">
                      <div class="bs-breadcrumb">
                          <ul class="breadcrumb">
                              <li><a href="#"><i class="fa fa-home" aria-hidden="true" style="color: white;font-size: 20px"></i></a></li>
                              <li><a href="#">Admission</a></li>
                              <li><a href="#">Admission Result</a></li>
                          </ul>
                      </div>
                  </div>
                  <div class="row">
                      <div class="well well-heading"><i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video</div>
                  </div>
                  <div class="row text-center margin-top10">
                      <iframe width="360" height="200" src="//www.youtube.com/embed/qEF1odHJgKI" frameborder="0" allowfullscreen>
                      </iframe>
                  </div>

                  <div class="row">
                    <div class="well well-heading"><i class="fa fa-windows" aria-hidden="true"></i> Related Topics</div>
                  </div>
                  <div class="row margin-bottom20">
                      <ul class="list-unstyled">
                          <li class="list-group-item"><a href="prospectus.php"><i class="fa fa-calendar" aria-hidden="true"> </i> Prospectus</a></li>
                          <li class="list-group-item"><a href="#"><i class="fa fa-table" aria-hidden="true"> </i> Admission Form</a></li>
                          <li class="list-group-item"><a href="admission-circular.php"><i class="fa fa-file-o" aria-hidden="true"> </i> Admission Circular</a></li>
                        
                          <li class="list-group-item"><a href="#"><i class="fa fa-columns" aria-hidden="true"> </i> Courses/Programs</a></li>
                      </ul>                   
              </div>
          </div>
        </div>              
    </div>
</div>
</div>


<?php include('inc/footer.php');?>