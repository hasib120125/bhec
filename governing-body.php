<?php include('inc/header.php');?>

<div class="container" style="margin-top: 10px;margin-bottom: 10px;">
	<div class="row">
		<div class="col-sm-8">
			<div class="row mp">
                <div class="panel-group">
				    <div class="panel panel-primary">
				      <div class="panel-heading p-head">Bangladesh Home Economics Collage Governing Body</div>
				      <div class="panel-body">
				      	<div class="margin-top10">
		                  	<div class="col-sm-12 governing-bottom">
		                    	<div class="col-sm-4 hidden-xs"></div>
			                    <div class="col-sm-4 governing-body-profile" align="center">
			                        <img src="images/chairman.jpeg" class="img-responsive text-center" alt="" title="">
			                        <span><strong>Brigadier General</strong></span><br>
			                        <span><strong>Willie M. Zane, BSP, awc, psc</strong></span><br>
			                        <span>Commander</span><br>
			                        <span>46 Independent Infantry Brigade</span><br>
			                        <span><strong>Chairman</strong></span>
			                    </div>
		                  	</div>
		                </div>

		                <div class="margin-top10">
		                    <div class="col-sm-12 governing-bottom" align="center">
		                      <div class="col-sm-4 governing-body-profile text-center">
		                        <img src="images/chairman.jpeg" class="img-responsive text-center" alt="" title="">
		                        <span><strong>Col Mohammad Mosleh Uddin, DCP, MCPS, FCPS</strong></span><br>
		                        <span>CMH, Dhaka Cantonment</span><br>
		                        <span><strong>Members</strong></span>
		                      </div>

		                      <div class="col-sm-4 governing-body-profile text-center">
		                        <img src="images/chairman.jpeg" class="img-responsive text-center" alt="" title="">
		                        <span><strong>Lt. Col Mohammad Golam Rabbani, psc</strong></span><br>
		                        <span>GSO-1 (OPS)</span><br>
		                        <span>Logistics Area, Dhaka Cantonment</span><br>
		                        <span><strong>Members</strong></span>
		                      </div>
		                      <div class="col-sm-4 governing-body-profile text-center">
		                        <img src="images/chairman.jpeg" class="img-responsive text-center" alt="" title="">
		                        <span><strong>Major Md. Anarul Kabir, Inf</strong></span><br>
		                        <span>SSO</span><br>
		                        <span>Station Headquarters, Dhaka Cantonment </span><br>
		                        <span><strong>Members</strong></span>
		                      </div>
		                    </div>
		                    <div class="col-sm-12 governing-bottom">
		                      <div class="col-sm-4 governing-body-profile text-center">
		                        <img src="images/chairman.jpeg" class="img-responsive text-center" alt="" title="">
		                        <span><strong>Md. Islmail Kabir, AEC</strong></span><br>
		                        <span>GSO-2 , Education Director</span><br>
		                        <span>AHQ</span><br>
		                        <span><strong>Members</strong></span>
		                      </div>
		                      <div class="col-sm-4 governing-body-profile text-center">
		                        <img src="images/chairman.jpeg" class="img-responsive text-center" alt="" title="">
		                        <span><strong>Md. Mustafizur Rahman</strong></span><br>
		                        <span>Join Secretary (Director Innovantion)</span><br>
		                        <span><strong>Members</strong></span>
		                      </div>
		                     
		                      <div class="col-sm-4 governing-body-profile text-center">
		                        <img src="images/chairman.jpeg" class="img-responsive text-center" alt="" title="">
		                        <span><strong>Dr. Nasima Khatun</strong></span><br>
		                        <span>Asst. Professor</span><br>
		                        <span>Dhaka Dental College</span><br>
		                        <span><strong>Members</strong></span>
		                      </div>
		                     </div>
		                    <div class="col-sm-12 governing-bottom">
		                      <div class="col-sm-4 governing-body-profile text-center">
		                        <img src="images/chairman.jpeg" class="img-responsive text-center" alt="" title="">
		                        <span><strong>Nahid Hossain Nobeli</strong></span><br>
		                        <span>Sr. Teacher</span><br>
		                        <span><strong>Members</strong></span>
		                      </div>

		                      <div class="col-sm-4 governing-body-profile text-center">
		                          <img src="images/chairman.jpeg" class="img-responsive text-center" alt="" title="">
		                          <span><strong>Md. Rafiqul Islam</strong></span><br>
		                          <span>Co-ordinator</span><br>
		                          <span><strong>Members</strong></span>
		                        </div>

		                        <div class="col-sm-4 text-center governing-body-profile">
		                          <img src="images/chairman.jpeg" class="img-responsive text-center" alt="" title="">
		                          <span><strong>Gazi Rebaca Rowshan</strong></span><br>
		                          <span>Sr. Teacher</span><br>
		                          <span><strong>Members</strong></span>
		                        </div>
		                    </div>               
				      </div>
				    </div>
				</div>
 			</div>

		</div>
		<div class="row mp" style="margin-top: 5px; color: #736AFF;font-weight: bold;">
			<div class="col-md-12 box">
		    Total Views : 2369 views
			</div>
        </div>
	</div>

		<div class="col-sm-4">
			<div class="row mp">
              <div class="col-xs-12 col-sm-12">
                    <div class="row">
	                    <div class="bs-breadcrumb">
	                        <ul class="breadcrumb">
	                            <li><a href="#"><i class="fa fa-home" aria-hidden="true" style="color: white;font-size: 20px"></i></a></li>
	                            <li><a href="#">Administration</a></li>
	                            <li><a href="#">Governing Body</a></li>
	                        </ul>
	                    </div>
                	</div>
		            <div class="row">
		                <div class="well well-heading"><i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video</div>
		            </div>
                  	<div class="row text-center margin-top10">
                      <iframe width="360" height="200" src="//www.youtube.com/embed/qEF1odHJgKI" frameborder="0" allowfullscreen>
                      </iframe>
                  	</div>

                  	<div class="row">
                  		<div class="well well-heading"><i class="fa fa-windows" aria-hidden="true"></i> Related Topics</div>
                	</div>
                  	<div class="row margin-bottom20">
	                    <ul class="list-unstyled">
	                      <li class="list-group-item"><a href="message-chairman.php"><i class="fa fa-history"></i> Message Chairman</a></li>
	                      <li class="list-group-item"><a href="message-vice-principle.php"><i class="fa fa-newspaper-o"></i> Message Principal</a></li>
	                      <li class="list-group-item"><a href="achievement.php"><i class="fa fa-university"></i> Message Vice Principal</a></li></li>
	                      <li class="list-group-item"><a href="teacher-info.php"><i class="fa fa-newspaper-o"></i> Teacher Information</a></li>
	                      <li class="list-group-item"><a href="staff-info.php"><i class="fa fa-newspaper-o"></i> Staff Information</a></li>
	                    </ul>                   
	                </div>
          		</div>
        	</div>
		</div>
	</div>
</div>




<?php include('inc/footer.php');?>