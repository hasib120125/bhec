<?php include('inc/header.php');?>


<div class="container" style="margin-top: 10px;margin-bottom: 10px;">
<div class="row mp">
  <div class="col-sm-8 details-left-panel">
    <div class="panel-group">
      <div class="panel panel-primary">
        <div class="panel-heading p-head">Bangladesh Home Economics Collage Courses/Programs</div>
        <div class="panel-body">
          <h4>বিভাগ</h4>
            <p>পাঁচটি বিভাগ হলঃ</p><br>
            <p>->গৃহ ব্যবস্থাপনা ও গৃহায়ন</p><br>
            <p>->শিশু বিকাশ ও সামাজিক সম্পর্ক</p><br>
            <p>->ব্যবহারিক শিল্পকলা</p><br>
            <p>->বস্ত্র পরিচ্ছদ ও বয়ন শিল্প</p>

          <h4>ভর্তি</h4>
            <p>ঢাকা বিশ্ববিদ্যালয়ের গার্হস্থ্য অর্থনীতি ইউনিট এ ভর্তি পরীক্ষা দিয়ে এই কলেজে ভর্তি হওয়া যায়। আসন সংখ্যা ৫৫০ টি</p>

          <h4>ক্যাম্পাস</h4>
            <p>৬ষ্ঠ তলা ভবনে কলেজটি ১৪৬/৪ গ্রীনরোড,(পানি উন্নয়ন বোর্ডের অপর দিকে) এ অবস্থিত।</p>
        </div>
      </div>
    </div>

    <div class="row mp" style="margin-top: 5px; color: #736AFF;font-weight: bold;">
      <div class="col-md-12 box">
        Total Views : 2369 views
      </div>
    </div>
    
  </div>
  <div class="col-sm-4 details-right-panel">
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <div class="row">
                      <div class="bs-breadcrumb">
                          <ul class="breadcrumb">
                              <li><a href="#"><i class="fa fa-home" aria-hidden="true" style="color: white;font-size: 20px"></i></a></li>
                              <li><a href="#">Admission</a></li>
                              <li><a href="#">Courses/Programs</a></li>
                          </ul>
                      </div>
                  </div>
                  <div class="row">
                      <div class="well well-heading"><i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video</div>
                  </div>
                  <div class="row text-center margin-top10">
                      <iframe width="360" height="200" src="//www.youtube.com/embed/qEF1odHJgKI" frameborder="0" allowfullscreen>
                      </iframe>
                  </div>

                  <div class="row">
                    <div class="well well-heading"><i class="fa fa-windows" aria-hidden="true"></i> Related Topics</div>
                  </div>
                  <div class="row margin-bottom20">
                      <ul class="list-unstyled">
                          <li class="list-group-item"><a href="prospectus.php"><i class="fa fa-calendar" aria-hidden="true"> </i> Prospectus</a></li>
                          <li class="list-group-item"><a href="admission-form.php"><i class="fa fa-table" aria-hidden="true"> </i> Admission Form</a></li>
                          <li class="list-group-item"><a href="admission-result.php"><i class="fa fa-file-o" aria-hidden="true"> </i> Admission Result</a></li>
                          <li class="list-group-item"><a href="admission-waiting-list.php"><i class="fa fa-list" aria-hidden="true"> </i> Waiting List</a></li>
                          <li class="list-group-item"><a href="#"><i class="fa fa-columns" aria-hidden="true"> </i> Courses/Programs</a></li>
                      </ul>                   
              </div>
          </div>
        </div>              
    </div>
</div>
</div>


<?php include('inc/footer.php');?>