<?php include('inc/header.php');?>


<div class="container" style="margin-top: 10px;margin-bottom: 10px;">
<div class="row mp">
  <div class="col-sm-8">
    <div class="panel-group">
      <div class="panel panel-primary">
        <div class="panel-heading p-head">Bangladesh Home Economics Collage Teachers</div>
        <div class="panel-body">
              <div class="row padding-left0 margin-top10">
                <div class="col-xs-3 col-sm-3 padding-left0"><img src="https://www.acps.edu.bd/media/teacher_staff/23-10-2016-1477198769.jpg" class="img-responsive img-teacher-info" alt="Md. Yahiya" title="Md. Yahiya"></div>
                <div class="col-xs-9 col-sm-9 teacher-info">
                    <div class="row">
                      <p><span class="col-xs-6"><strong>Name</strong></span><span class="col-xs-6"><strong>Md. Yahiya</strong></span></p>
                      <p><span class="col-xs-6">Department</span><span class="col-xs-6">Agricultural Studies</span></p>
                      <p><span class="col-xs-6">Designation</span><span class="col-xs-6">Sr. Teacher</span></p>
                   
                      <p><span class="col-xs-6">Subject</span><span class="col-xs-6">Agricultural Studies</span></p>
                  
                      <p><span class="col-xs-6">ID</span><span class="col-xs-6">14119501</span></p>
                    </div>
                </div>
              </div>

              <div class="row padding-left0 margin-top10">
                <div class="col-xs-3 col-sm-3 padding-left0"><img src="https://www.acps.edu.bd/media/teacher_staff/23-10-2016-1477198769.jpg" class="img-responsive img-teacher-info" alt="Md. Yahiya" title="Md. Yahiya"></div>
                <div class="col-xs-9 col-sm-9 teacher-info">
                    <div class="row">
                      <p><span class="col-xs-6"><strong>Name</strong></span><span class="col-xs-6"><strong>Md. Yahiya</strong></span></p>
                      <p><span class="col-xs-6">Department</span><span class="col-xs-6">Agricultural Studies</span></p>
                      <p><span class="col-xs-6">Designation</span><span class="col-xs-6">Sr. Teacher</span></p>
                   
                      <p><span class="col-xs-6">Subject</span><span class="col-xs-6">Agricultural Studies</span></p>
                  
                      <p><span class="col-xs-6">ID</span><span class="col-xs-6">14119501</span></p>
                    </div>
                </div>
              </div>

              <div class="row padding-left0 margin-top10">
                <div class="col-xs-3 col-sm-3 padding-left0"><img src="https://www.acps.edu.bd/media/teacher_staff/23-10-2016-1477198769.jpg" class="img-responsive img-teacher-info" alt="Md. Yahiya" title="Md. Yahiya"></div>
                <div class="col-xs-9 col-sm-9 teacher-info">
                    <div class="row">
                      <p><span class="col-xs-6"><strong>Name</strong></span><span class="col-xs-6"><strong>Md. Yahiya</strong></span></p>
                      <p><span class="col-xs-6">Department</span><span class="col-xs-6">Agricultural Studies</span></p>
                      <p><span class="col-xs-6">Designation</span><span class="col-xs-6">Sr. Teacher</span></p>
                   
                      <p><span class="col-xs-6">Subject</span><span class="col-xs-6">Agricultural Studies</span></p>
                  
                      <p><span class="col-xs-6">ID</span><span class="col-xs-6">14119501</span></p>
                    </div>
                </div>
              </div>

              <div class="row padding-left0 margin-top10">
                <div class="col-xs-3 col-sm-3 padding-left0"><img src="https://www.acps.edu.bd/media/teacher_staff/23-10-2016-1477198769.jpg" class="img-responsive img-teacher-info" alt="Md. Yahiya" title="Md. Yahiya"></div>
                <div class="col-xs-9 col-sm-9 teacher-info">
                    <div class="row">
                      <p><span class="col-xs-6"><strong>Name</strong></span><span class="col-xs-6"><strong>Md. Yahiya</strong></span></p>
                      <p><span class="col-xs-6">Department</span><span class="col-xs-6">Agricultural Studies</span></p>
                      <p><span class="col-xs-6">Designation</span><span class="col-xs-6">Sr. Teacher</span></p>
                   
                      <p><span class="col-xs-6">Subject</span><span class="col-xs-6">Agricultural Studies</span></p>
                  
                      <p><span class="col-xs-6">ID</span><span class="col-xs-6">14119501</span></p>
                    </div>
                </div>
              </div>
        </div>
      </div>
    </div>

      <div class="row margin-top10">
        <div class="col-sm-12 col-md-4">
          <ul class="pagination">
            <li><a href="#"> &laquo; </a></li>
              <li class="active"><a href="#"> 1 </a></li>
              <li><a href="#"> 2 </a></li>
              <li><a href="#"> 3 </a></li>
              <li><a href="#"> 4 </a></li>
              <li class="disabled"><a href="#"> &raquo; </a></li>
          </ul>
        </div>
      </div>
  </div>
  <div class="col-sm-4 details-right-panel">
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <div class="row">
                    <div class="bs-breadcrumb">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home" aria-hidden="true" style="color: white;font-size: 20px"></i></a></li>
                            <li><a href="#">Administration</a></li>
                            <li><a href="#">Teacher Information</a></li>
                        </ul>
                    </div>
                  </div>

                  <div class="row">
                    <div class="well well-heading"><i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video</div>
                  </div>

                  <div class="row text-center margin-top10">
                      <iframe width="360" height="200" src="//www.youtube.com/embed/qEF1odHJgKI" frameborder="0" allowfullscreen>
                      </iframe>
                  </div>

                  <div class="row">
                      <div class="well well-heading"><i class="fa fa-windows" aria-hidden="true"></i> Departments</div>
                  </div>

                  <div class="row">
                      <ul class="list-unstyled">
                        <li class="list-group-item"><a href="food-and-nutrition.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Food & Nutrition</a></li>

                        <li class="list-group-item"><a href="child-development.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Child Development & Social Relationship</a></li>

                        <li class="list-group-item"><a href="resource-management.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Resource Management & Entrepreneurship</a></li>

                        <li class="list-group-item"><a href="arts-studies.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Art & Creative Studies</a></li>

                        <li class="list-group-item"><a href="clothing-textile.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Clothing & Textile</a></li>

                      </ul>
                  </div>

                  <div class="row">
                      <div class="well well-heading"><i class="fa fa-windows" aria-hidden="true"></i> Related Topics</div>
                  </div>
                  <div class="row margin-bottom20">
                      <ul class="list-unstyled">
                        <li class="list-group-item"><a href="governing-body.php"><i class="fa fa-newspaper-o"></i> Governing Body</a></li>
                        <li class="list-group-item"><a href="message-chairman.php"><i class="fa fa-history"></i> Message Chairman</a></li>
                        <li class="list-group-item"><a href="message-vice-principle.php"><i class="fa fa-newspaper-o"></i> Message Principal</a></li>
                        <li class="list-group-item"><a href="achievement.php"><i class="fa fa-university"></i> Message Vice Principal</a></li></li>
                        <li class="list-group-item"><a href="staff-info.php"><i class="fa fa-newspaper-o"></i> Staff Information</a></li>
                      </ul>                   
                  </div>
          </div>
        </div>              
    </div>
</div>
</div>


<?php include('inc/footer.php');?>