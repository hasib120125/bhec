<?php include('inc/header.php');?>

<div class="container" style="margin-top: 10px;margin-bottom: 10px;">
	<div class="row">
		<div class="col-md-8">
        <div class="row mp">              
            <div class="col-xs-12 col-sm-12 box mar-top10">
                <div class="col-sm-7"><h1 class="head-h1">Academic Notice of Bangladesh Home Economics Collage, Dhaka </h1></div>
                  <form name="notice" action="#" method="post">
                    <div class="col-sm-3 mp">
                        <select name="cmb_cat" class="form-control">
                          <option value="1">Office</option>
                          <option value="2">Examination</option>
                          <option value="3">Accounts</option>
                        </select>
                    </div>
                    <div class="col-sm-2"><input type="submit" name="search" value="Search" class="form-control btn-primary pull-right">
                    </div>
                  </form>
          </div>
        </div>
        <div class="col-sm-12 box mar-top10 notice-hover">
          <div class="col-sm-2"><strong>Publish Date</strong></div>
          <div class="col-sm-6"><strong>Notice Title</strong></div>
          <div class="col-sm-2"><strong>Department</strong></div>
          <div class="col-sm-1"><strong>View</strong></div>
          <div class="col-sm-1"><strong>Down.</strong></div>
        </div>
        <div class="col-sm-12 box mar-top10 notice-hover">
          <div class="col-sm-2">8 Mar, 2018</div>
          <div class="col-sm-6">Monthly Tuition and other fees have to be paid through online banking system from 1 April 2018.</div>
          <div class="col-sm-2">Office</div>
          <div class="col-sm-1"><a href="notice-details.php" target="_blank"><img src="images/view-icon.png"></a></div>
          <div class="col-sm-1"><a href="notice-download?file=07-03-2018-1520481105.pdf"><i class="fa fa-cloud-download i-txt"></i></a></div>
        </div>
        <div class="col-sm-12 box mar-top10 notice-hover">
          <div class="col-sm-2">17 Dec, 2017</div>
          <div class="col-sm-6">Photograph Uploading Procedure</div>
          <div class="col-sm-2">Office</div>
          <div class="col-sm-1"><a href="notice-details.php" target="_blank"><img src="images/view-icon.png"></a></div>
          <div class="col-sm-1"><a href="notice-download?file=24-01-2018-1516847454.pdf"><i class="fa fa-cloud-download i-txt"></i></a></div>
        </div>
        <div class="col-sm-12 box mar-top10 notice-hover">
          <div class="col-sm-2">11 Sep, 2017</div>
          <div class="col-sm-6">Online Payment Instruction</div>
          <div class="col-sm-2">Office</div>
          <div class="col-sm-1"><a href="notice-details.php" target="_blank"><img src="images/view-icon.png"></a></div>
          <div class="col-sm-1"><a href="notice-download?file=18-09-2017-1505717461.pdf"><i class="fa fa-cloud-download i-txt"></i></a></div>
        </div>
        <div class="col-sm-12 box mar-top10 notice-hover">
          <div class="col-sm-2">21 Jun, 2017</div>
          <div class="col-sm-6">Session Charge and other fees of Class XI &amp; XII - 2017</div>
          <div class="col-sm-2">Office</div>
          <div class="col-sm-1"><a href="notice-details.php" target="_blank"><img src="images/view-icon.png"></a></div>
          <div class="col-sm-1"><a href="notice-download?file=21-06-2017-1498026253.pdf"><i class="fa fa-cloud-download i-txt"></i></a></div>
        </div>
        <div class="col-sm-12 box mar-top10 notice-hover">
          <div class="col-sm-2">5 Mar, 2017</div>
          <div class="col-sm-6">Syllabus and Marks Distribution for PECE-2017</div>
          <div class="col-sm-2">Office</div>
          <div class="col-sm-1"><a href="notice-details.php" target="_blank"><img src="images/view-icon.png"></a></div>
           <div class="col-sm-1"><a href="notice-download?file=05-03-2017-1488711723.pdf"><i class="fa fa-cloud-download i-txt"></i></a></div>
        </div>
        <div class="col-sm-12 box mar-top10 notice-hover">
          <div class="col-sm-2">5 Mar, 2017</div>
          <div class="col-sm-6">Syllabus and Marks Distribution for PECE-2017</div>
          <div class="col-sm-2">Office</div>
          <div class="col-sm-1"><a href="notice-details.php" target="_blank"><img src="images/view-icon.png"></a></div>
           <div class="col-sm-1"><a href="notice-download?file=05-03-2017-1488711723.pdf"><i class="fa fa-cloud-download i-txt"></i></a></div>
        </div>
        <div class="col-sm-12 box mar-top10 notice-hover">
          <div class="col-sm-2">5 Mar, 2017</div>
          <div class="col-sm-6">Syllabus and Marks Distribution for PECE-2017</div>
          <div class="col-sm-2">Office</div>
          <div class="col-sm-1"><a href="notice-details.php" target="_blank"><img src="images/view-icon.png"></a></div>
           <div class="col-sm-1"><a href="notice-download?file=05-03-2017-1488711723.pdf"><i class="fa fa-cloud-download i-txt"></i></a></div>
        </div>
        <div class="col-sm-12 box mar-top10 notice-hover">
          <div class="col-sm-2">5 Mar, 2017</div>
          <div class="col-sm-6">Syllabus and Marks Distribution for PECE-2017</div>
          <div class="col-sm-2">Office</div>
          <div class="col-sm-1"><a href="notice-details.php" target="_blank"><img src="images/view-icon.png"></a></div>
           <div class="col-sm-1"><a href="notice-download?file=05-03-2017-1488711723.pdf"><i class="fa fa-cloud-download i-txt"></i></a></div>
        </div>

        <div class="row margin-top10">
            <div class="col-sm-12 col-md-4">
              <ul class="pagination">
                  <li><a href="#"> &laquo; </a></li>
                  <li class="active"><a href="#"> 1 </a></li>
                  <li><a href="#"> 2 </a></li>
                  <li><a href="#"> 3 </a></li>
                  <li><a href="#"> 4 </a></li>
                  <li class="disabled"><a href="#"> &raquo; </a></li>
              </ul>
            </div>
        </div>
                        
      </div>

      <div class="col-sm-4 details-right-panel">
          <div class="row mp">
              <div class="col-xs-12 col-sm-12">
                  <div class="row">
                      <div class="bs-breadcrumb">
                          <ul class="breadcrumb">
                              <li><a href="#"><i class="fa fa-home" aria-hidden="true" style="color: white;font-size: 20px"></i></a></li>
                              <li><a href="#">Notice</a></li>
                              <li><a href="#">General Notice</a></li>
                          </ul>
                      </div>
                  </div>
                  <div class="row">
                      <div class="well well-heading"><i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video</div>
                  </div>
                  <div class="row text-center margin-top10">
                      <iframe width="360" height="200" src="//www.youtube.com/embed/qEF1odHJgKI" frameborder="0" allowfullscreen>
                      </iframe>
                  </div>

                  <div class="row">
                    <div class="well well-heading"><i class="fa fa-windows" aria-hidden="true"></i> Related Topics</div>
                  </div>
                  <div class="row margin-bottom20">
                      <ul class="list-unstyled">
                          <li class="list-group-item"><a href="#"><i class="fa fa-calendar" aria-hidden="true"> </i> Food & Nutrition</a></li>
                          <li class="list-group-item"><a href="#"><i class="fa fa-table" aria-hidden="true"> </i> Child Development & Social Relationship</a></li>
                          <li class="list-group-item"><a href="#"><i class="fa fa-file-o" aria-hidden="true"> </i> Resource Management & Entrepreneurship</a></li>
                          <li class="list-group-item"><a href="content?newstype=5"><i class="fa fa-list" aria-hidden="true"> </i> Art & Creative Studies</a></li>
                          <li class="list-group-item"><a href="#"><i class="fa fa-columns" aria-hidden="true"> </i> Clothing & Textile</a></li>
                      </ul>                   
              </div>
          </div>
        </div>              
    </div>
	</div>
</div>


<?php include('inc/footer.php');?>